// These following functions should be called by NWNX plugin developers, who should expose
// their own, more friendly headers.
//
// For example, this following function would wrap a call which passes three parameters,
// receives three back, and constructs a vector frm the result.
//
//     vector GetVectorFromCoords(float x, float y, float z)
//     {
//         string pluginName = "NWNX_TestPlugin";
//         string funcName = "GiveMeBackTheSameValues";
//
//         // Note the inverse argument push order.
//         // C++-side, arguments will be consumed from right to left.
//         NWNX_PushArgumentFloat(pluginName, funcName, z);
//         NWNX_PushArgumentFloat(pluginName, funcName, y);
//         NWNX_PushArgumentFloat(pluginName, funcName, x);
//
//         // This calls the function, which will prepare the return values.
//         NWNX_CallFunction(pluginName, funcName);
//
//         // C++-side pushes the return values in reverse order so we can consume them naturally here.
//         float _x = NWNX_GetReturnValueFloat(pluginName, funcName);
//         float _y = NWNX_GetReturnValueFloat(pluginName, funcName);
//         float _z = NWNX_GetReturnValueFloat(pluginName, funcName);
//
//         return vector(_x, _y, _z);
//     }

namespace NWN
{
    public class NWNX
    {
        static public void NWNX_CallFunction(string pluginName, string functionName)
        {
            NWNX_INTERNAL_CallFunction(pluginName, functionName);
        }

        static public void NWNX_PushArgumentInt(string pluginName, string functionName, int value)
        {
            NWNX_INTERNAL_PushArgument(pluginName, functionName, "0 " + NWScript.IntToString(value));
        }

        static public void NWNX_PushArgumentFloat(string pluginName, string functionName, float value)
        {
            NWNX_INTERNAL_PushArgument(pluginName, functionName, "1 " + NWScript.FloatToString(value));
        }

        static public void NWNX_PushArgumentObject(string pluginName, string functionName, object value)
        {
            NWNX_INTERNAL_PushArgument(pluginName, functionName, "2 " + NWScript.ObjectToString(value as NWN.Object));
        }

        static public void NWNX_PushArgumentString(string pluginName, string functionName, string value)
        {
            NWNX_INTERNAL_PushArgument(pluginName, functionName, "3 " + value);
        }

        static public int NWNX_GetReturnValueInt(string pluginName, string functionName)
        {
            return NWScript.StringToInt(NWNX_INTERNAL_GetReturnValueString(pluginName, functionName, "0 "));
        }

        static public float NWNX_GetReturnValueFloat(string pluginName, string functionName)
        {
            return NWScript.StringToFloat(NWNX_INTERNAL_GetReturnValueString(pluginName, functionName, "1 "));
        }

        static public object NWNX_GetReturnValueObject(string pluginName, string functionName)
        {
            return NWNX_INTERNAL_GetReturnValueObject(pluginName, functionName, "2 ");
        }

        static public string NWNX_GetReturnValueString(string pluginName, string functionName)
        {
            return NWNX_INTERNAL_GetReturnValueString(pluginName, functionName, "3 ");
        }

        static public void NWNX_INTERNAL_CallFunction(string pluginName, string functionName)
        {
            NWScript.SetLocalString(NWScript.GetModule(), "NWNXEE!CALL_FUNCTION!" + pluginName + "!" + functionName, "1");
        }

        static public void NWNX_INTERNAL_PushArgument(string pluginName, string functionName, string value)
        {
            NWScript.SetLocalString(NWScript.GetModule(), "NWNXEE!PUSH_ARGUMENT!" + pluginName + "!" + functionName, value);
        }

        static public string NWNX_INTERNAL_GetReturnValueString(string pluginName, string functionName, string type)
        {
            return NWScript.GetLocalString(NWScript.GetModule(), "NWNXEE!GET_RETURN_VALUE!" + pluginName + "!" + functionName + "!" + type);
        }

        static public object NWNX_INTERNAL_GetReturnValueObject(string pluginName, string functionName, string type)
        {
            return NWScript.GetLocalObject(NWScript.GetModule(), "NWNXEE!GET_RETURN_VALUE!" + pluginName + "!" + functionName + "!" + type);
        }
    }
}
namespace NWN
{
    public class NWNXEvents
    {
        static public void NWNX_Events_SubscribeEvent(string evt, string script)
        {
            NWNX.NWNX_PushArgumentString("NWNX_Events", "SUBSCRIBE_EVENT", script);
            NWNX.NWNX_PushArgumentString("NWNX_Events", "SUBSCRIBE_EVENT", evt);
            NWNX.NWNX_CallFunction("NWNX_Events", "SUBSCRIBE_EVENT");
        }

        static public void NWNX_Events_PushEventData(string tag, string data)
        {
            NWNX.NWNX_PushArgumentString("NWNX_Events", "PUSH_EVENT_DATA", data);
            NWNX.NWNX_PushArgumentString("NWNX_Events", "PUSH_EVENT_DATA", tag);
            NWNX.NWNX_CallFunction("NWNX_Events", "PUSH_EVENT_DATA");
        }

        static public int NWNX_Events_SignalEvent(string evt, object target)
        {
            NWNX.NWNX_PushArgumentObject("NWNX_Events", "SIGNAL_EVENT", target);
            NWNX.NWNX_PushArgumentString("NWNX_Events", "SIGNAL_EVENT", evt);
            NWNX.NWNX_CallFunction("NWNX_Events", "SIGNAL_EVENT");
            return NWNX.NWNX_GetReturnValueInt("NWNX_Events", "SIGNAL_EVENT");
        }

        static public string NWNX_Events_GetEventData(string tag)
        {
            NWNX.NWNX_PushArgumentString("NWNX_Events", "GET_EVENT_DATA", tag);
            NWNX.NWNX_CallFunction("NWNX_Events", "GET_EVENT_DATA");
            return NWNX.NWNX_GetReturnValueString("NWNX_Events", "GET_EVENT_DATA");
        }
    }
}
using System;
using System.Threading;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;
using NWN;
using System.Configuration;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace NWN.Scripts
{
    public class character_mover
    {
        static bool initialized = false;
        const string servervault_path = "/home/potm/.local/share/Neverwinter Nights/servervault/";
        const string servervault_backup_path = "/home/potm/backup/servervault/";
        const string backup_path = "/home/potm/mover-backups/";

        struct CharacterMoveDescription
        {
            public string source_account;
            public string target_account;
            public string character_file;
        }

        static ConcurrentStack<CharacterMoveDescription> move_stack = new ConcurrentStack<CharacterMoveDescription>();

        static public int Main ()
        {
            // On the first call to this script, we start a background thread to process tasks.
            if (!initialized)
            {
                Console.WriteLine("Not initialized yet, creating worker thread..");

                initialized = true;

                Thread mover_thread = new Thread(MoveCharactersThread);
                mover_thread.IsBackground = true;
                mover_thread.Start();
            }

            // We construct a parametrized task for the background worker thread to handle.
            CharacterMoveDescription move_params = new CharacterMoveDescription();
            try 
            {
                NWN.Object move_object = Object.OBJECT_SELF;
                move_params.source_account = (NWScript.GetLocalString(move_object, "source_account") as string);
                move_params.target_account = (NWScript.GetLocalString(move_object, "target_account") as string);
                move_params.character_file = (NWScript.GetLocalString(move_object, "character_file") as string);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error moving character!");
                Console.WriteLine(e);
                return 1;
            }
            Console.WriteLine("Pushing params..");
            move_stack.Push(move_params);

            return 1;
        }

        static public void MoveCharactersThread()
        {
            // Since this is a background thread, it will close when the main thread closes, so we can loop infinitely.
            while (true)
            {
                CharacterMoveDescription move_params;
                if (move_stack.TryPop(out move_params))
                {
                    Console.WriteLine("Moving " + move_params.character_file + " from " + move_params.source_account + " to " + move_params.target_account);
                    Console.WriteLine(move_params.source_account);
                    Console.WriteLine(move_params.target_account);
                    Console.WriteLine(move_params.character_file + ".bic");

                    // Don't move the character if a file with the same name already exists in the target location.
                    if (File.Exists(servervault_path + move_params.target_account + "/" + move_params.character_file + ".bic"))
                    {
                        Console.WriteLine("Could not move character - target character already exists.");
                        continue;
                    }

                    try
                    {
                        Directory.CreateDirectory(servervault_path + move_params.target_account);
                        // Before moving, backup the character file just in case something has gone wrong.
                        // The backup subdirectory includes datetime to make backups more unique in case
                        // there's a problem related to someone creating several characters with same name
                        // on the same account to try to exploit the system.
                        // DateTime.Now.ToString("yyyyMMddHHmmss");
                        var time_string = DateTime.Now.ToString("yyyyMMddHHmmss");
                        Directory.CreateDirectory(backup_path + move_params.target_account + "-" + time_string);
                        File.Copy(servervault_path + move_params.source_account + "/" + move_params.character_file + ".bic",
                            backup_path + move_params.target_account + "-" + time_string + "/" + move_params.character_file + ".bic");

                        Directory.CreateDirectory(servervault_path + move_params.target_account);
                        File.Move(servervault_path + move_params.source_account + "/" + move_params.character_file + ".bic",
                            servervault_path + move_params.target_account + "/" + move_params.character_file + ".bic");

                        try
                        {
                            Directory.CreateDirectory(servervault_backup_path + move_params.target_account);
                            File.Move(servervault_backup_path + move_params.source_account + "/" + move_params.character_file + ".bic",
                                servervault_backup_path + move_params.target_account + "/" + move_params.character_file + ".bic");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error moving the backup file. Perhaps it hasn't been created yet?");
                            Console.WriteLine(e);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error moving character!");
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    // Sleep for half a second to avoid busy-looping.
                    Thread.Sleep(500);
                }
            }
        }
    }
}
